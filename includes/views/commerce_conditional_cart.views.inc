<?php

/**
 * Provide shopping cart related Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_conditional_cart_views_data_alter(&$data) {
  // Define some fields based upon views_handler_field_entity in the entity
  // table so they can be re-used with other query backends.
  // @see views_handler_field_entity

  $data['commerce_product']['add_to_cart_form']['moved to'] = array('views_entity_commerce_product', 'add_to_cart_form');
  $data['views_entity_commerce_product']['conditional_add_to_cart_form'] = array(
    'field' => array(
      'title' => t('Conditional  Add to Cart form'),
      'help' => t('Conditionaly display an Add to Cart form for the product.'),
      'handler' => 'commerce_conditional_cart_handler_field_conditional_add_to_cart_form',
    ),
  );
}
