<?php

/**
 * Field handler to present an add to cart form for the product..
 */
class commerce_conditional_cart_handler_field_conditional_add_to_cart_form extends commerce_cart_handler_field_add_to_cart_form {

  function option_definition() {
    $options = parent::option_definition();
    $options['commerce_conditional_cart_add_to_cart_field'] = array('default' => FALSE);
    $options['commerce_conditional_cart_add_to_cart_operator'] = array('default' => FALSE);
    $options['commerce_conditional_cart_add_to_cart_value'] = array('default' => FALSE);
    $options['commerce_conditional_cart_add_to_cart_display_choice'] = array('default' => FALSE);
    $options['commerce_conditional_cart_add_to_cart_custom_text'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide the add to cart display options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['commerce_conditional_cart_add_to_cart_field'] = array(
      '#type' => 'select',
      '#title' => t('Select a field to check against to conditionally display the cart.'),
      '#options' => $this->get_products_fields(),
      '#default_value' => !empty($this->options['commerce_conditional_cart_add_to_cart_field']) ? $this->options['commerce_conditional_cart_add_to_cart_field'] : FALSE,
    );
    $form['commerce_conditional_cart_add_to_cart_operator'] = array(
      '#type' => 'select',
      '#title' => t('Select an operator to use.'),
      '#options' => array('==' => '=', '<' => '<', '>' => '>'),
      '#default_value' => !empty($this->options['commerce_conditional_cart_add_to_cart_operator']) ? $this->options['commerce_conditional_cart_add_to_cart_operator'] : FALSE,
    );
    $form['commerce_conditional_cart_add_to_cart_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value it should match.'),
      '#default_value' => !empty($this->options['commerce_conditional_cart_add_to_cart_value']) ? $this->options['commerce_conditional_cart_add_to_cart_value'] : FALSE,
    );
    $form['commerce_conditional_cart_add_to_cart_display_choice'] = array(
      '#type' => 'radios',
      '#title' => t('If the condition does not match'),
      '#options' => array('disable_cart' => 'Disable the cart form', 'custom_text' => 'Display a custom text'),
      '#default_value' => !empty($this->options['commerce_conditional_cart_add_to_cart_display_choice']) ? $this->options['commerce_conditional_cart_add_to_cart_display_choice'] : 'disable_cart',
    );
    $form['commerce_conditional_cart_add_to_cart_custom_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter the custom text to be displayed.'),
      '#default_value' => !empty($this->options['commerce_conditional_cart_add_to_cart_custom_text']) ? $this->options['commerce_conditional_cart_add_to_cart_custom_text'] : FALSE,
      '#required' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="options[commerce_conditional_cart_add_to_cart_display_choice]"]' => array('value' => 'custom_text'),
        )
      ),
    );
  }

  function get_products_fields() {
    $product_type = commerce_product_types();
    foreach (field_info_fields() as $field_name => $field) {
      if (!empty($field['bundles']['commerce_product'])) {
        $list[$field_name] = $field_name;
      }
    }
    ksort($list);
    return $list;
  }

  function render($values) {
    $product = $this->get_value($values);
    if (empty($product)) {
      return;
    }
    $cart_form = $this->build_cart_form($product);
    if ($this->options['commerce_conditional_cart_add_to_cart_field'] == FALSE) {
      return drupal_render($cart_form);
    }
    $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
    $product_wrapper->{$this->options['commerce_conditional_cart_add_to_cart_field']}->Value();
    $field_value = $product_wrapper->{$this->options['commerce_conditional_cart_add_to_cart_field']}->Value();

    if (!empty($this->options['commerce_conditional_cart_add_to_cart_display_choice']) && $this->options['commerce_conditional_cart_add_to_cart_display_choice'] == 'custom_text') {
      $bool = $this->conditional_operator_test($field_value);
      if ($bool) {
        $variables = array(
          'custom_text' => check_plain($this->options['commerce_conditional_cart_add_to_cart_custom_text']),
        );
        return theme('conditional_cart_custom_text', $variables);
      }
      else {
        return drupal_render($cart_form);
      }
    }
    // Disabled the cart.
    else {
      $bool = $this->conditional_operator_test($field_value);
      if ($bool) {
        // Override the submit button and disable it.
        $cart_form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Product not available'),
          '#weight' => 50,
          '#attributes' => array('disabled' => 'disabled'),
          '#validate' => array('commerce_cart_add_to_cart_form_disabled_validate'),
        );
        return drupal_render($cart_form);
      }
      else {
        return drupal_render($cart_form);
      }
    }
  }

  /**
   * Run the conditional tests.
   *
   * Return a boolean value. True if the tests matches, otherwise, FALSE.
   */
  function conditional_operator_test($field_value) {
    $operator = $this->options['commerce_conditional_cart_add_to_cart_operator'];
    $bool = FALSE;
    switch ($operator) {
      case '==':
        $bool = $field_value == $this->options['commerce_conditional_cart_add_to_cart_value'] ? FALSE : TRUE;
        break;

      case '<':
        $bool = $field_value < $this->options['commerce_conditional_cart_add_to_cart_value'] ? FALSE : TRUE;
        break;

      case '>':
        $bool = $field_value > $this->options['commerce_conditional_cart_add_to_cart_value'] ? FALSE : TRUE;
        break;
    }
    return $bool;
  }

  /**
   * Build a form array that contains the cart.
   *
   * @param $product
   * @return array|mixed
   */
  function build_cart_form($product) {
    // Extract a default quantity for the Add to Cart form line item.
    $default_quantity = $this->options['default_quantity'] <= 0 ? 1 : $this->options['default_quantity'];
    $product_ids = array($product->product_id);

    // Build the line item we'll pass to the Add to Cart form.
    $line_item = commerce_product_line_item_new($product, $default_quantity, 0, array(), $this->options['line_item_type']);
    $line_item->data['context']['product_ids'] = $product_ids;
    $line_item->data['context']['add_to_cart_combine'] = $this->options['combine'];

    // Generate a form ID for this add to cart form.
    $form_id = commerce_cart_add_to_cart_form_id($product_ids, $default_quantity);

    // Add the display path to the line item's context data array if specified.
    if ($this->options['display_path']) {
      if ($this->view->display[$this->view->current_display]->display_plugin == 'page') {
        $line_item->data['context']['display_path'] = $this->view->display[$this->view->current_display]->display_options['path'];
      }
      else {
        $line_item->data['context']['display_path'] = current_path();
      }
    }

    // Store the View data in the context data array as well.
    $line_item->data['context']['view'] = array(
      'view_name' => $this->view->name,
      'display_name' => $this->view->current_display,
      'human_name' => $this->view->human_name,
      'page' => $this->view->current_page,
    );
    // Build the Add to Cart form using the prepared values.
    $form = drupal_get_form($form_id, $line_item, $this->options['show_quantity'], array());
    return $form;
  }
}
